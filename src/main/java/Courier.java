import java.util.Objects;

public class Courier {

    private String login;
    private String password;
    private String firstName;
    private Boolean ok;
    private String message;
    private Integer id;

    public Courier(String login, String password, String firstName, Boolean ok, String message, Integer id) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.ok = ok;
        this.message = message;
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Courier() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Courier courier = (Courier) o;
        return Objects.equals(login, courier.login) && Objects.equals(password, courier.password) && Objects.equals(firstName, courier.firstName) && Objects.equals(ok, courier.ok);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, password, firstName, ok);
    }
}
