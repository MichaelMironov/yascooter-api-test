import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class CreateOrder extends BaseTest {

    @DataProvider
    public static Object[][] orderDates(){
        return new Object[][]{
                {"Михаил", "Алексеев", "Ул. Полимерная", 3, "+7 999 999 99 99", 5, "2022-07-11", "Комментарий к заказу", "BLACK"},
                {"Иван", "Иванов", "Ул. Марксистская", 3, "+7 999 999 99 99", 5, "2022-07-25", "Комментарий к заказу", "GREY"},
                {"Алексей", "Петров", "Ул. Ленина", 3, "+7 999 999 99 99", 5, "2022-06-15", "Комментарий к заказу", "GREY, BLACK"},
                {"Виктор", "Чернышов", "Ул. Ленина", 3, "+7 999 999 99 99", 5, "2022-06-15", "Комментарий к заказу", ""}
        };
    }


    @Test(dataProvider = "orderDates")
    public void createOrderTest(String firstname, String lastname, String address, Integer metro, String phone, Integer rent, String delivery, String comment, String[] color){
        Order order = new Order();

        order.setFirstName(firstname);
        order.setFirstName(lastname);
        order.setAddress(address);
        order.setMetroStation(metro);
        order.setPhone(phone);
        order.setRentTime(rent);
        order.setDeliveryDate(delivery);
        order.setComment(comment);
        order.setColor(color);


        order = given()
                .body(order)
                .baseUri("https://qa-scooter.praktikum-services.ru/api/v1/orders")
                .when().post()
                .then().statusCode(201)
                .log().everything()
                .extract().as(Order.class);

        Assert.assertNotNull(order.getTrack());
    }

    @Test
    public void gerOrdersList(){
        given()
                .baseUri("https://qa-scooter.praktikum-services.ru/api/v1/orders")
                .when().get()
                .then().statusCode(200)
                .log().everything();
    }
}
