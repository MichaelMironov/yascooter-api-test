import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeClass;
import java.io.IOException;
import static io.restassured.RestAssured.*;

public class BaseTest {

    @BeforeClass
    @Description("Setup configuration")
    public void setup() throws IOException {
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("config.properties"));
        String uri = System.getProperty("uri");
        if(uri == null || uri.isEmpty()){
            throw new RuntimeException("В файле \"config.properties\" отсутствует значение \"uri\"");
        }

        baseURI = uri;
        Specifications.installSpecification(Specifications.requestSpec(baseURI));

    }
    @Step("Generate dates for courier")
    public static Courier generateDate(Courier courier){

        courier.setLogin(RandomStringUtils.randomAlphabetic(10));
        courier.setPassword(RandomStringUtils.randomAlphabetic(10));
        courier.setFirstName(RandomStringUtils.randomAlphabetic(10));

        return courier;
    }

}