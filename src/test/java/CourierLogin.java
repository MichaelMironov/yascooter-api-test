import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class CourierLogin extends BaseTest {

    @Test
    public void courierLoginTest(){

        Courier courier = new Courier();

        courier.setLogin("xMtcwaAQav");
        courier.setPassword("JEkmVdkALT");

        courier = given()
                .body(courier)
                .basePath("/login")
                .when().post()
                .then()
                .statusCode(200)
                .log().all()
                .extract().as(Courier.class);

        Assert.assertNotNull(courier.getId());
    }

    @Test
    public void courierLoginWithoutRequiredField(){

        Courier courier = new Courier();

        courier.setPassword("12345");

        given()
                .body(courier)
                .basePath("/login")
                .when().post()
                .then().statusCode(400)
                .log().everything();
    }

    @Test
    public void nonExistentCourier(){

        Courier courier = new Courier();

        courier.setLogin("093");
        courier.setPassword("--");

        given()
                .body(courier)
                .basePath("/login")
                .when().post()
                .then().statusCode(404)
                .log().everything();
    }
}
