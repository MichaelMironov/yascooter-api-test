import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class CourierCreation extends BaseTest {

    @Test
    public void registerNewCourier() {

        Courier courier = new Courier();
        generateDate(courier);

                given()
                        .body(courier)
                        .when()
                        .post(baseURI)
                        .then().statusCode(201)
                        .log().all()
                        .extract().body()
                        .as(Courier.class);

    }

     @Test
    public void courierRegistrationWithoutRequiredField(){

        Courier courier = new Courier();

        courier.setFirstName("Test");
        courier.setLogin("negative_test");

        given()
                .body(courier)
                .when().post(baseURI)
                .then().statusCode(400)
                        .log().everything();

     }

     @Test
    public void courierReRegistration(){

         Courier courier = new Courier();
         generateDate(courier);

         given()
                 .body(courier)
                 .when()
                 .post(baseURI)
                 .then().statusCode(201);


         given()
                 .body(courier)
                 .when()
                 .post(baseURI)
                 .then().statusCode(409)
                 .log().everything();
     }
}
